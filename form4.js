const inputName = document.getElementById("campoNombre");
const inputContrasena = document.getElementById("Password");
const inputConfirmacion = document.getElementById("campoConfirmarContrasena");
const submitBtn = document.querySelector('input[type="submit"]');
const submiConfir = document.querySelector(".confir");

console.log(inputName, submitBtn);

submitBtn.addEventListener("click", validar_nombre);
function validar_nombre(e) {
    e.preventDefault();
    let letter = /^[A-Za-z\s]+$/;

    if(inputName.value.length <=4 || inputName.value.length > 30) {
        alert("Nombre muy pequeño")
    } else {
        if(inputName.value.match(letter)) {
            alert("Your name was accepted");
        } else {
            alert("Please input alphabet characters only");
        }
    
    }
    
}

console.log(inputContrasena, inputConfirmacion, submiConfir);

submiConfir.addEventListener("click", validar_contrasenas);
function validar_contrasenas(e) {
    e.preventDefault();
    if(inputContrasena.value == "" || inputConfirmacion.value == ""){
        alert("No pueden haber cadenas vacías");
    } else {
        if(inputContrasena.value.length != inputConfirmacion.value.length){
            alert("Longitud de contraseña incorrecta");
        } else {
            if(inputContrasena.value != inputConfirmacion.value){
                alert("Contraseñas no coinciden");
            } else {
                alert("Contraseña confirmada");
            }
        }
    }
}

